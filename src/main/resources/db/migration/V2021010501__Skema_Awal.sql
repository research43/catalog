create table product (
    id varchar(36),
    code varchar(50) not null,
    name varchar(100) not null,
    price decimal(19,2) not null,
    primary key (id),
    unique (code)
);